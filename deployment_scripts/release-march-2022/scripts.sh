aws cloudformation create-stack  --stack-name ec2-intstancecreation-stack --template-body file://C:/Users/nithina/Desktop/Ec2InstanceCreation.yml  --parameters file://C:/Users/nithina/Desktop/Ec2InstanceCreationParams.json


aws cloudformation create-stack  --stack-name lambda-intstancdetails-stack --template-body file://InstanceDetailsLambda.yml  --parameters file://InstanceDetailsLambdaParams.json --capabilities CAPABILITY_NAMED_IAM

aws iam attach-user-policy --policy-arn arn:aws:iam:793330927009:aws:policy/AdministratorAccess --user-role Q_DocParserDevRole

aws cloudformation create-stack  --stack-name cognito-intstancdetails-stack --template-body file://InstanceDetailsCognitoPool.yml  --parameters file://InstanceDetailsCognitoPoolParams.json 

aws cloudformation create-stack  --stack-name apigw-intstancdetails-stack --template-body file://InstanceDetailsApiGateway.yml  --parameters file://InstanceDetailsApiGWParams.json 
