AWSTemplateFormatVersion: 2010-09-09
Description: CFS to create the EC2 instance in the provide VPC

Parameters:
  EnvironmentType:
    Description: "Specify the Environment type of the stack."
    Type: String
    Default: dev
    AllowedValues:
      - dev
      - test
      - prod
  AmiID:
    Type: String
    Description: "The ID of the AMI."
    Default: ami-0e1d30f2c40c4c701
  KeyPairName:
    Type: String
    Description: The name of an existing Amazon EC2 key pair in this region to use to SSH.

Mappings:
  EnvironmentToInstanceType:
    dev:
      InstanceType: t2.nano
    test:
      InstanceType: t2.micro
    prod:
      InstanceType: t2.small

Resources:

  UserManagementVpc:
      Type: AWS::EC2::VPC
      Properties:
        CidrBlock: "10.0.0.0/22"
        Tags:
          - Key: "Name"
            Value: "UserManagementVpc"
            
  PubSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: us-east-1a
      CidrBlock: 10.0.0.0/24
      MapPublicIpOnLaunch: false
      Tags:
        - Key: 'Name'
          Value: 'PubSubnetA'
      VpcId: !Ref UserManagementVpc     

  IGW:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: "Name"
          Value: "UserManagementIGW"
          
  IgwAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref IGW
      VpcId: !Ref UserManagementVpc
   
  MainRT:
    Type: AWS::EC2::RouteTable
    Properties:
      Tags:
        - Key: "Name"
          Value: "MainRT"
      VpcId: !Ref UserManagementVpc
   
  MainRTRoute:
    Type: AWS::EC2::Route
    DependsOn: IgwAttachment
    Properties:
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref IGW
      RouteTableId: !Ref MainRT 

  MainRTSubnetAAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref MainRT
      SubnetId: !Ref PubSubnetA
        
  WebAppInstance:
    Type: AWS::EC2::Instance
    Properties:
      AvailabilityZone: us-east-1a
      ImageId: !Ref AmiID
      InstanceType:
        !FindInMap [
          EnvironmentToInstanceType,
          !Ref EnvironmentType,
          InstanceType,
        ]
      KeyName: !Ref KeyPairName
      SecurityGroupIds:
        - !Ref InstanceSecurityGroup
      SubnetId: !Ref PubSubnetA
      BlockDeviceMappings:
        - DeviceName: /dev/sda1
          Ebs:
            VolumeSize: 8
            
  InstanceSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: !Join ["-", [server-security-group, !Ref EnvironmentType]]
      GroupDescription: "Allow HTTP/HTTPS and SSH inbound and outbound traffic"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 0.0.0.0/0
      SecurityGroupEgress:
        - CidrIp: 0.0.0.0/0
          IpProtocol: "-1"
      VpcId:
        Ref: UserManagementVpc
   
  InstanceEIP:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc
      InstanceId: !Ref WebAppInstance
      Tags:
        - Key: Name
          Value: !Join ["-", [server-eip, !Ref EnvironmentType]]

Outputs:
  WebsiteURL:
    Value: !Sub http://${InstanceEIP}
    Description: WebApp URL
